import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AddUserComponent} from './add-user/add-user.component';
import {ListUsersComponent} from './list-users/list-users.component';
import {RouterModule, Routes} from '@angular/router';


export const routes: Routes = [
  {
    path: 'list',
    component: ListUsersComponent
  },
  {
    path: 'add',
    component: AddUserComponent
  },
  {
    path: '',
    redirectTo: 'list',
    pathMatch: 'full'
  }
];


@NgModule({
  declarations: [AddUserComponent, ListUsersComponent],
  imports: [
    SharedModuleModule,
    CommonModule,
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class UsersModule { }
