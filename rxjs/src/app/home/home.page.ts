import {Component, OnDestroy, OnInit} from '@angular/core';
import {Observable, of, Subject, Subscription} from 'rxjs';
import {catchError, distinctUntilChanged, map, tap} from 'rxjs/operators';

const fakeUser: any = {
  idUser: 0,
  name: 'Manolo',
  surname: 'Pérez',
  email: 'email@email.com',
  idGroup: null,
  idNav: null,
  idRole: null,
  idSacmi: null,
  sacmiUser: true,
  disabled: false
};

const fakeListUsers = [
  {...fakeUser},
  {...fakeUser, name: 'Jose', surname: 'Gómez', idNav: '140', idGroup: 'HORNOS'},
  {...fakeUser, name: 'Luis', surname: 'Gómez', idNav: '162', idGroup: 'LGV'}
];

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit, OnDestroy {

  listUsers: any[];
  subscription: Subscription;

  constructor() {
  }


  ngOnInit(): void {
    this.exampleNewObservableSubject();
    this.subscription = of(fakeListUsers)
      .pipe(
        tap(list => console.log(list)),
        map(list => {
            return [...list, {...list[0], invented: 'YEEE'}]
            // throw new Error('error interno');
          }
        ),
        tap(list => console.log(list)),
        catchError(err => {
          return of([]);
        })
      ).subscribe(
        (list) => {
          this.listUsers = list;
        }
      );
  }

  exampleNewObservableSubject() {
    const subject = new Subject();
    subject.pipe(
      distinctUntilChanged()
    );

    const observable = subject.asObservable();
    subject.next('envío datos');


    const subscription = observable.pipe(
      tap(x => console.log(x))
    ).subscribe(
      value => console.log('suscrito:', value),
      error => console.log(error),
      () => console.log('completado')
    );

    subject.next('envío datos 2º');

    const observableNew = new Observable(subscriber => {
      subscriber.error(new Error('Error new'));
      subscriber.next('Datos new');
      subscriber.complete();
    });

    subject.next('envío datos 3º');
    subject.next([1, 1]);
    //subject.error(new Error('ERROR'));
    subject.complete();
    subscription.unsubscribe();
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
