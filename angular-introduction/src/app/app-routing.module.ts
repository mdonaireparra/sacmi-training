import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {HelloComponent} from './pages/hello/hello.component';
import {NoWelcomeComponent} from './pages/no-welcome/no-welcome.component';

const routes: Routes = [
  {
    path: "hello",
    component: HelloComponent
  },
  {
    path: "no-welcome",
    component: NoWelcomeComponent
  },
  {
    path: "",
    redirectTo: "hello",
    pathMatch: "full"
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
