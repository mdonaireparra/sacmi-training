import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-no-welcome',
  templateUrl: './no-welcome.component.html',
  styleUrls: ['./no-welcome.component.scss']
})
export class NoWelcomeComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
