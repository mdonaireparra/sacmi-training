import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NoWelcomeComponent } from './no-welcome.component';

describe('NoWelcomeComponent', () => {
  let component: NoWelcomeComponent;
  let fixture: ComponentFixture<NoWelcomeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NoWelcomeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NoWelcomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
