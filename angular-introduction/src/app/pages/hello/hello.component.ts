import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-hello',
  templateUrl: './hello.component.html',
  styleUrls: ['./hello.component.scss']
})
export class HelloComponent implements OnInit {
  title = 'angular-introduction';

  constructor() { }

  ngOnInit(): void {
  }

  onTitleClick($event: any) {
    console.log($event);
  }
}
