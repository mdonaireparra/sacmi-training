import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {HelloComponent} from './pages/hello/hello.component';
import {ToolbarComponent} from './components/toolbar/toolbar.component';
import {NoWelcomeComponent} from './pages/no-welcome/no-welcome.component';

@NgModule({
  declarations: [
    AppComponent,
    HelloComponent,
    ToolbarComponent,
    NoWelcomeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
